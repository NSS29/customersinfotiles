////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.web.controller;
////////////////////////////////////////////////////////////////////////////////
import com.nss.customersinfo.dao.CustomerDAO;
import com.nss.customersinfo.model.Customer;
import java.io.*;
import java.util.*;
import javax.validation.Valid;
import org.springframework.context.MessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@Controller
public class CustomerController {
    
    @Autowired
    MessageSource messageSource;

    @Autowired
    private CustomerDAO customerDAO;

    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String list(Model uiModel) {
        List<Customer> customers = customerDAO.findAll();
        uiModel.addAttribute("customers", customers);
        return "customers/list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") Long id, Model uiModel) {
        Customer customer = customerDAO.findByID(id);
        uiModel.addAttribute("customer", customer);
        return "customers/show";
    }

    @RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("customer", customerDAO.findByID(id));
        return "customers/edit";
    }
    
    @RequestMapping(value = "/", params = "form", method = RequestMethod.GET)
    public String createForm(Model uiModel) {
        Customer customer = new Customer();
        uiModel.addAttribute("customer", customer);
        return "customers/edit"; 
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String updateOrInsert(@ModelAttribute("customer") @Valid Customer customer,
            BindingResult bindingResult, Model uiModel, Locale locale) { 
        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("message", messageSource.getMessage("contact_save_fail", new Object[]{}, locale));        	
            uiModel.addAttribute("customer", customer);
            return "customers/edit";
        }
        if(customer.getId() == null) {
            customerDAO.insert(customer);
        } else {
            customerDAO.update(customer);
        } 
        return "redirect:/";
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id, Model uiModel) {
        customerDAO.delete(customerDAO.findByID(id));
        return "redirect:/";
    }  
//------------------------------------------------------------------------------ 
    @RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
    public String loginerror(Model uiModel) {
        uiModel.addAttribute("error", "true");
        return "denied";
    }
 
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model uiModel) {
        return "logout";
    }
//------------------------------------------------------------------------------
}
////////////////////////////////////////////////////////////////////////////////