////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.dao.hibernate;
////////////////////////////////////////////////////////////////////////////////
import com.nss.customersinfo.dao.CustomerDAO;
import com.nss.customersinfo.model.Customer;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@Repository("customerDAO")
public class CustomerDAOImpl extends CustomerDAO {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(readOnly=true)
    public List<Customer> findAll() {    
    return sessionFactory.getCurrentSession()
            .createQuery("from Customer c").list();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Customer findByID(Long id) {
        return (Customer) sessionFactory.getCurrentSession()
                .get(Customer.class, id);
    }
    
    @Override
    @Transactional
    public void update(Customer customer) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("UPDATE Customer SET lastName=:lastName,"
                + "firstName=:firstName, birthDate=:birthDate, email=:email,"
                + "description=:description WHERE id=:id");
        query.setParameter("lastName", customer.getLastName());
        query.setParameter("firstName", customer.getFirstName());
        query.setParameter("birthDate", customer.getBirthDate());
        query.setParameter("email", customer.getEmail());
        query.setParameter("description", customer.getDescription());
        query.setParameter("id", customer.getId());
        query.executeUpdate();
    }

    @Override
    @Transactional
    public void insert(Customer customer) {
        sessionFactory.getCurrentSession().persist(customer);
    }

    @Override
    @PreAuthorize("(isAuthenticated() and hasRole('ROLE_USER') or hasRole('ROLE_ADMIN'))")
    @Transactional
    public void delete(Customer customer) {
        sessionFactory.getCurrentSession().delete(customer);
    }
}
////////////////////////////////////////////////////////////////////////////////