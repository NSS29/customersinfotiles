////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.model;
////////////////////////////////////////////////////////////////////////////////
import static javax.persistence.GenerationType.IDENTITY;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
////////////////////////////////////////////////////////////////////////////////
@Entity
@Table(name = "customer")
public class Customer extends BaseEntity implements Serializable {
    
    @NotEmpty
    @Size(min=3, max=10)
    @Column(name = "LAST_NAME")
    private String lastName;
    
    @NotEmpty
    @Size(min=5, max=10)
    @Column(name = "FIRST_NAME")
    private String firstName;
    
    @Column(name = "BIRTH_DATE")
    private String birthDate;
    
    @Pattern(regexp=".+@.+\\.[a-z]+")
    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "DESCRIPTION")
    private String description;

    public Customer() {}
    public Customer(String lastName, String firstName, String birthDate, 
            String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
    }
    
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        Customer c = (Customer) obj;
        return (this.getLastName().equals(c.getLastName())
                && this.getFirstName().equals(c.getFirstName())
                && this.getBirthDate().equals(c.getBirthDate())
                && this.getEmail().equals(c.getEmail())
                && this.getDescription().equals(c.getDescription()));
    }
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 13;
        result = PRIME * result
                + getLastName().hashCode()
                + getFirstName().hashCode()
                + getBirthDate().hashCode()
                + getEmail().hashCode()
                + getDescription().hashCode();
        return result;
    }
    @Override
    public String toString() {
        return new StringBuilder().append("Contact - Id: ").append(getId())
                .append(", Last name: ").append(lastName)
                .append(", First name: ").append(firstName)
                .append(", Birthday: ").append(birthDate)
                .append(", Description: ").append(description).toString();
    }
}
////////////////////////////////////////////////////////////////////////////////