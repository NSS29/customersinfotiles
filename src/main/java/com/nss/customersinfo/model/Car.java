////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.model;
////////////////////////////////////////////////////////////////////////////////
import java.io.*;
import java.util.*;
import javax.persistence.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@Entity
@Table(name = "car")
public class Car extends BaseEntity implements Serializable {
    
    @Column(name = "COMPANY")
    private String company;
    
    @Column(name = "MODEL")
    private String model;
    
    @Column(name = "BODY")
    private String bodyType;
    
    @Column(name = "STATUS")
    private String status;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "CONSTRUCTED")
    private Date dateConstruction;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "customer_car",
            joinColumns = { @JoinColumn(name = "CAR_ID") },
            inverseJoinColumns = { @JoinColumn(name="CUSTOMER_ID") })
    private Set<Customer> customers = new LinkedHashSet();
    
    
    public Car() {}
    
    public Car(String company, String model, String bodyType,
            String status) {
        this.company = company;
        this.model = model;
        this.bodyType = bodyType;
        this.status = status;
    }
    public Car(String company, String model, String bodyType,
            String status, Date dateConstruction) {
        this(company, model, bodyType, status);
        this.dateConstruction = dateConstruction;
    }

    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {   
        this.company = company;
    }
    
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    
    public String getBodyType() {
        return bodyType;
    }
    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
    public Date getDateConstruction() {
        return dateConstruction;
    }
    public void setDateConstruction(Date dateConstruction) {
        this.dateConstruction = dateConstruction;
    }
    
    public Set<Customer> getCustomers() {
        return customers;
    }
    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
            
        Car c = (Car) obj;
        return (getCompany().equalsIgnoreCase(c.getCompany())
                && getModel().equalsIgnoreCase(c.getModel())
                && getBodyType().equalsIgnoreCase(c.getBodyType())
                && getStatus().equalsIgnoreCase(c.getStatus())
                && getDateConstruction().equals(c.getDateConstruction()));
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 13;
        result = PRIME * result + getCompany().hashCode()
                +getModel().hashCode()+getBodyType().hashCode()
                +getStatus().hashCode()+getDateConstruction().hashCode();
        return result;
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append(getModel())
                .append("(").append(getStatus()).append(")").toString();
    }
}
////////////////////////////////////////////////////////////////////////////////